# Client of WACK
This is the mobile end of [Wack](https://bitbucket.org/bgmemo/wack) project which is written in Angular.js and Ionic Framework.

## The following tools are adopted:

* Angular.js

* Restangular

* Ionic (Mobile Framework)

* ngCordova (Angular services of Cordova)

---
![alt tag](https://s3-ap-southeast-2.amazonaws.com/gkg5037/jedi_pics/cafes.png)

---
![alt tag](https://s3-ap-southeast-2.amazonaws.com/gkg5037/jedi_pics/more.png)

---
![alt tag](https://s3-ap-southeast-2.amazonaws.com/gkg5037/jedi_pics/services.png)

---
![alt tag](https://s3-ap-southeast-2.amazonaws.com/gkg5037/jedi_pics/shop_details.png)

---
![alt tag](https://s3-ap-southeast-2.amazonaws.com/gkg5037/jedi_pics/shop_new.png)

---


