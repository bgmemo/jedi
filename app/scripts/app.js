'use strict';

/**
 * Initialize lodash
 *
 */
var lodash = angular.module('lodash', []);

lodash.factory('_', function() {
  return window._;
});


/**
 * Initialize application
 */
var app = angular.module('loqlApp', ['ionic', 'ngCordova', 'restangular', 'lodash', 'config', 'pasvaz.bindonce']);

app.filter('distance', function() {
  return function(input) {
    if (input >= 1000) {
      return (input / 1000).toFixed(1) + '千米';
    } else {
      return input + '米';
    }
  };
});

// app.run(function($ionicTabsConfig) {
//   // Default to ios tab-bar style on android too
//   $ionicTabsConfig.type = '';
// })

app.config(function(ENV, RestangularProvider) {

  RestangularProvider.setBaseUrl(ENV.apiEndpoint);

  RestangularProvider.setDefaultHeaders({
    // 'Content-Type': 'application/json'
    // 'Content-Type': 'application/x-www-form-urlencoded'
  });

  RestangularProvider.setDefaultRequestParams({
    format: 'jsonp'
  });

  RestangularProvider.setDefaultRequestParams('jsonp', {
    callback: 'JSON_CALLBACK'
  });

  // Map MongoDB default index
  RestangularProvider.setRestangularFields({
    id: '_id'
  });


  // RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
  //   console.log("data: " + data);
  //   console.log("operation: " + operation);
  //   console.log("what: " + what);
  //   console.log("url: " + url);
  //   console.log("response: " + response);
  //   console.log("deferred: " + deferred);
  // });
  //


  // RestangularProvider.addFullRequestInterceptor(function(headers, params, element, httpConfig) {
  // console.log("headers: " + headers);
  // console.log("params: " + params);
  // console.log("element: " + element);
  // console.log("httpConfig: " + httpConfig);
  // });


  RestangularProvider.addElementTransformer('users', true, function(user) {
    user.addRestangularMethod('autoregister', 'post', 'autoregister');

    return user;
  });


  // RestangularProvider.addElementTransformer('shops', true, function(shop) {
  //   shop.addRestangularMethod('near', 'post', 'near');
  //   return shop;
  // });

});
