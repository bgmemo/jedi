/*global app*/
'use strict';

/**
 * Config app router
 *
 * @param  {Service} $stateProvider
 * @param  {Service} $urlRouterProvider
 *
 * @return
 */
app.config(function router($stateProvider, $urlRouterProvider) {

  // Config of Routes
  $urlRouterProvider.otherwise('/tab/home');

  // Abstract tab
  $stateProvider.state('tabs', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  });

  // Home page
  $stateProvider.state('tabs.home', {
    url: '/home',
    views: {
      'homeTab': {
        templateUrl: 'templates/home.html',
        resolve: {
          currentLocation: function($q, GeoService) {
            var deferred = $q.defer();

            GeoService.getLocation().then(function(location) {
              deferred.resolve(location);
            });

            return deferred.promise;
          }
        },
        controller: 'HomeCtrl'
      }
    }
  });

  // Second page to getting nearest cafes
  $stateProvider.state('tabs.cafe', {
    url: '/cafe',
    views: {
      'cafeTab': {
        templateUrl: 'templates/cafe.html',
        resolve: {
          nearCafes: function($window, ShopService, $q) {
            var deferred = $q.defer();

            var lng = $window.sessionStorage.getItem('lng'),
              lat = $window.sessionStorage.getItem('lat');

            ShopService.getCafes(lng, lat).then(function(cafes) {
              deferred.resolve(cafes);
            })

            return deferred.promise;
          }
        },
        controller: 'CafeCtrl'
      }
    }
  });

  $stateProvider.state('tabs.service', {
      url: '/service',
      views: {
        'serviceTab': {
          templateUrl: 'templates/service.html',
          controller: 'ServiceCtrl'
        }
      }
    })
    .state('tabs.news', {
      url: '/news',
      views: {
        'newsTab': {
          templateUrl: 'templates/news.html',
          controller: 'NewsCtrl'
        }
      }
    })
    .state('tabs.more', {
      url: '/more',
      views: {
        'moreTab': {
          templateUrl: 'templates/more.html'
        }
      }
    });

  // Shop
  $stateProvider
    .state('tabs.list', {
      url: '/service/:id',
      views: {
        'serviceTab': {
          templateUrl: 'templates/shop/list.html',
          controller: 'ShopListCtrl'
        }
      }
    })
    .state('tabs.cafeprofile', {
      url: '/cafe/:id',
      views: {
        'cafeTab': {
          templateUrl: 'templates/shop/cafe-profile.html',
          controller: 'ShopProfileCtrl'
        }
      }
    })
    .state('tabs.shopprofile', {
      url: '/shop/:id',
      views: {
        'serviceTab': {
          templateUrl: 'templates/shop/shop-profile.html',
          controller: 'ShopProfileCtrl'
        }
      }
    })
    .state('tabs.updateshop', {
      url: '/updateshop/:id',
      views: {
        'serviceTab': {
          templateUrl: 'templates/shop/update.html',
          controller: 'ShopUpdateCtrl'
        }
      }
    })
    .state('tabs.addshop', {
      url: '/addshop',
      views: {
        'moreTab': {
          templateUrl: 'templates/shop/add.html',
          controller: 'ShopAddCtrl'
        }
      }
    });

  // Others
  $stateProvider
    .state('tabs.feedback', {
      url: '/feedback',
      views: {
        'moreTab': {
          templateUrl: 'templates/feedback.html',
          controller: 'FeedbackCtrl'
        }
      }
    })
    .state('tabs.aboutus', {
      url: '/aboutus',
      views: {
        'moreTab': {
          templateUrl: 'templates/aboutus.html',
          controller: 'FeedbackCtrl'
        }
      }
    });

});