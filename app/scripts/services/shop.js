'use strict';
/**
 * Service of shop associated
 */
app.service('ShopService', ['Restangular', '$q', function(Restangular, $q) {

  var shopService = this;
  var cafeCID;
  var cafes;

  this.getCafes = function(lng, lat) {
    var deferred = $q.defer();
    if (this.getCafeCategoryId()) {
      if (shopService.cafes) {
        deferred.resolve(shopService.cafes);
      } else {
        Restangular.one("categories", this.getCafeCategoryId()).all('shops').customPOST({
          lng: lng,
          lat: lat
        }, "near").then(function(res) {
          shopService.cafes = res;
          deferred.resolve(res);
        })
      }
    } else {
      throw new Exception('no Cafe id found.')
    }

    return deferred.promise;
  }



  this.getCafeCategoryId = function() {
    var deferred = $q.defer();
    if (cafeCID) {
      deferred.resolve(cafeCID);
    } else {
      Restangular.all("categories").getList().then(function(categories) {
        cafeCID = shopService.getCafeCID(categories);
        deferred.resolve(cafeCID);
      });
    }
    return deferred.promise;
  }

  this.getCafeCID = function(categories) {
    for (var i = 0, len = categories.length; i < len; i++) {
      if (categories[i].name === '餐厅' || categories[i].name === '餐馆') {
        return categories[i]._id;
      }
    }
    return null;
  }



  this.search = function(filter) {
    var deferred = $q.defer();

    var matches = Restangular.all('shops').getList({
      search: filter
    });

    deferred.resolve(matches);

    return deferred.promise;
  };


}])