'use strict';
/**
 * Restangular Factories
 */
app.factory('Shops', ['Restangular',
  function(Restangular) {

    return Restangular.service('shops');
  }
])



app.factory('TmpShops', ['Restangular',
  function(Restangular) {

    return Restangular.service('tmpshops');
  }
])



app.factory('Categories', ['Restangular',
  function(Restangular) {

    return Restangular.service('categories');
  }
])



app.factory('Feedbacks', ['Restangular',
  function(Restangular) {

    return Restangular.service('feedbacks');
  }
])



app.factory('Users', ['Restangular',
  function(Restangular) {

    return Restangular.service('users');
  }
])