'use strict';
/**
 *
 */
app.service('GeoService', ['$q', '$cordovaGeolocation',
  function($q, $cordovaGeolocation) {

    /**
     * [getLocation description]
     * @return {[type]} [description]
     */
    this.getLocation = function() {
      var deferred = $q.defer();

      try {
        if (navigator.geolocation) {

          // HTML5 Geolocation
          var options = {
            timeout: 20000,
            enableHighAccuracy: true,
            maximumAge: 10000
          };

          navigator.geolocation.getCurrentPosition(function(res) {
            deferred.resolve(res);
          }, function(error) {}, options);
        } else {

          // Cordova Geolocation
          $cordovaGeolocation.getCurrentPosition().then(function(res) {
            deferred.resolve(res);
          }, function(err) {});
        }

        return deferred.promise;
      } catch (err) {}
    }
  }

])