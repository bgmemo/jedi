app.controller('ServiceCtrl', ['$scope', '$state', 'Categories', 'ShopService', '_', '$ionicHistory',
  function($scope, $state, Categories, ShopService, _, $ionicHistory) {

    Categories.getList().then(function(categories) {
      $scope.categories = categories;
    });

    $scope.showProfile = function(shopId) {
      $state.transitionTo('tabs.shopprofile', {
        id: shopId
      });
    }

    $scope.showService = function(categoryId) {
      $state.transitionTo('tabs.list', {
        id: categoryId
      });
    }

    $scope.data = {
      "searchResults": [],
      "search": ''
    };

    $scope.search = function() {
      var query = $scope.data.search;

      if (query) {
        ShopService.search(query).then(
          function(matches) {
            $scope.data.searchResults = matches;
          })
      } else {
        $scope.data.searchResults = [];
      }
    }

    $scope.clearSearchBox = function() {
      $scope.data.search = '';
      $scope.data.searchResults = [];
    }

    $ionicHistory.clearHistory();
  }
])





app.controller('ShopListCtrl', ['$scope', '$state', '$stateParams',
  '$ionicLoading', 'Restangular', 'ShopService', '$window',
  function($scope, $state, $stateParams, $ionicLoading, Restangular, ShopService, $window) {

    $scope.loadingIndicator = $ionicLoading.show({
      content: 'Loading...',
      animation: 'fade-in',
      showBackdrop: false,
      maxWidth: 200,
      showDelay: 500
    });

    var categoryId = $stateParams.id;

    Restangular.one("categories", categoryId).get().then(function(category) {
      $scope.category = category;
    });

    Restangular.one("categories", categoryId).all('shops').getList().then(function(shops) {
      $scope.shops = shops;
      $ionicLoading.hide();
    });


    // $scope.near = function() {
    //   ShopService.getCafeCategoryId().then(function(categoryId) {
    //     Restangular.one('categories', categoryId).all('shops')
    //       .customPOST({
    //         id: categoryId,
    //         lng: $window.localStorage.getItem('lng'),
    //         lat: $window.localStorage.getItem('lat')
    //       }, "near")
    //       .then(function(res) {
    //         $scope.shops = res; //Each shop has an additional 'dis' property
    //       })
    //       $ionicLoading.hide();
    //   })
    // }

    // $scope.near(); //Default get by nearest

    $scope.showProfile = function(shopid) {
      $state.transitionTo('tabs.shopprofile', {
        id: shopid
      });
    }

    $scope.GotoLink = function(address) {
      var url = 'http://maps.google.com?q=' + address;
      window.open(url, '_system');
    }

    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };

    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };


  }
])