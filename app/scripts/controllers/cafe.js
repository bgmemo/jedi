'use strict';
/**
 * Controller of Cafe tab
 */
app.controller('CafeCtrl', ['$scope', '$state', 'ShopService', 'nearCafes',
  '$window', '$ionicHistory',
  function($scope, $state, ShopService, nearCafes, $window, $ionicHistory) {

    $scope.shops = nearCafes;

    $scope.showProfile = function(shopid) {
      $state.transitionTo('tabs.cafeprofile', {
        id: shopid
      });
    }

    $scope.GotoLink = function(address) {
      var url = 'http://maps.google.com?q=' + address;
      window.open(url, '_system');
    }

    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };

    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };


    $ionicHistory.clearHistory();
  }
])