/*global app */
'use strict';

/**
 * Controller of Home tab
 */
app.controller('HomeCtrl', ['$scope', '$state', 'currentLocation',
  '$window', 'AuthService', '$cordovaSplashscreen',
  '$cordovaDevice', '$cordovaNetwork', '$ionicHistory',
  function($scope, $state, currentLocation,
    $window, AuthService, $cordovaSplashscreen,
    $cordovaDevice, $cordovaNetwork, $ionicHistory) {
    var lng = currentLocation.coords.longitude,
      lat = currentLocation.coords.latitude;

    $scope.device = {
      uuid: 'bypass',
      cordova: 'null'
    };

    try {
      $scope.device = $cordovaDevice.getDevice();
      $cordovaSplashscreen.hide();

      $scope.type = $cordovaNetwork.getNetwork();
      $scope.isOnline = $cordovaNetwork.isOnline();
      $scope.isOffline = $cordovaNetwork.isOffline();
    } catch (err) {}

    $window.sessionStorage.setItem('lng', lng);
    $window.sessionStorage.setItem('lat', lat);
    $scope.lng = lng;
    $scope.lat = lat;

    // AuthService.fetchToken($scope.device);
    $scope.token = $window.sessionStorage.Token;

    $ionicHistory.clearHistory();
  }
]);

/**
 * [description]
 * @param  {[type]} $scope       [description]
 * @param  {[type]} $window      [description]
 * @param  {[type]} Users        [description]
 * @param  {[type]} Restangular) {               $scope.signin [description]
 * @return {[type]}              [description]
 */
app.controller('UserCtrl', ['$scope', '$window', 'Users', 'Restangular',
  function($scope, $window, Users, Restangular) {

    $scope.signin = function(user) {
      Users.post(user).then(function(token) {
        $window.sessionStorage && $window.sessionStorage.setItem('token', token.replace(/['"]+/g, ''));
        Restangular.configuration.defaultHeaders.token = token;
      })
    }
  }

]);