'use strict';

app.controller('ShopProfileCtrl', ['$state', '$scope',
  '$stateParams', '$ionicLoading', 'Shops', 'Categories',
  function($state, $scope, $stateParams, $ionicLoading, Shops, Categories) {

    var shopid = $stateParams.id;

    Shops.one(shopid).get().then(function(shop) {
      $scope.shop = shop;

      Categories.one(shop.category).get().then(function(category) {
        $scope.category = category;
      });
    })

    $scope.updateShop = function(shopId) {
      $state.transitionTo('tabs.updateshop', {
        id: shopId
      });
    }

    $scope.GotoLink = function(address) {
      var url = 'http://maps.google.com?q=' + address;
      window.open(url, '_system');
    }

  }
])



app.controller('ShopUpdateCtrl', ['$state', '$scope', '$stateParams',
  '$ionicLoading', '$ionicPopup', 'Shops', 'TmpShops', 'Categories',
  function($state, $scope, $stateParams, $ionicLoading, $ionicPopup, Shops, TmpShops, Categories) {

    var shopId = $stateParams.id;

    Shops.one(shopId).get().then(function(shop) {
      $scope.shop = shop;
    })

    Categories.getList().then(function(categories) {
      $scope.categories = categories;
    });

    $scope.toggleChange = function() {}

    $scope.updateShop = function(shop) {
      //Remove unnessacery properties from shop object
      delete shop['_id'];
      delete shop['created_at'];
      delete shop['created_by'];

      TmpShops.post(shop).then(function(httpStatus) {
        if (httpStatus === 'Created') {
          var alertPopup = $ionicPopup.alert({
            title: '后台核准中',
            template: '核准后，商铺会在12小时内添加。'
          });

          alertPopup.then(function(res) {
            $state.transitionTo('tabs.more');
          });

        } else {}
      })
    }

  }
])




app.controller('ShopAddCtrl', ['$scope', '$state', '$ionicPopup', 'Categories', 'TmpShops',
  function($scope, $state, $ionicPopup, Categories, TmpShops) {

    $scope.shop = {};
    $scope.shop.created_by = 'OWNER';

    Categories.getList().then(function(categories) {
      $scope.categories = categories;
    });

    $scope.toggleChange = function() {}

    $scope.addShop = function(shop) {
      // If shop.email format is incorrect, it would be null
      TmpShops.post(shop).then(function(httpStatus) {
        if (httpStatus === 'Created') {
          var alertPopup = $ionicPopup.alert({
            title: '后台核准中',
            template: '核准后，商铺会在12小时内添加。'
          });

          alertPopup.then(function(res) {
            $state.transitionTo('tabs.more');
          });
        } else {}
      })
    }
  }
])


//