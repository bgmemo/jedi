app.controller('FeedbackCtrl', [
  '$scope', '$state', '$ionicPopup', 'Feedbacks', '$ionicHistory',
  function($scope, $state, $ionicPopup, Feedbacks, $ionicHistory) {

    $scope.postFeedback = function(email, content) {
      Feedbacks.post({
        email: email,
        content: content
      }).then(function(httpStatus) {
        //http_code = 201
        if (httpStatus === 'Created') {

          var alertPopup = $ionicPopup.alert({
            title: '感谢您的反馈',
            template: '我们会尽快回复你的意见。'
          });

          alertPopup.then(function(res) {
            $state.transitionTo('tabs.more');
          });

        } else {}
      })
    }

    $ionicHistory.clearHistory();
  }
])