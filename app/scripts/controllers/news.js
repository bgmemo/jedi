app.controller('NewsCtrl', [
  '$scope', '$state', '$ionicPopup', '$ionicHistory',
  function($scope, $state, $ionicPopup, $ionicHistory) {

    $scope.GotoLink = function(address) {
      var url = address;
      window.open(url, '_system');
    }

    $ionicHistory.clearHistory();
  }
])